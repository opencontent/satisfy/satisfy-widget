export const environment = {
  production: false,
  api: 'api/v1/ratings',
  api_config_rest: 'api/rest',
  api_config_gh: 'https://satisfy.opencityitalia.it/v1/graphql',
  style: 1
};
