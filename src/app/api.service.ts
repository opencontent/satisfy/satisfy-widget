import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { Apollo, gql } from 'apollo-angular';

@Injectable({
  providedIn: 'root',
})

export class ApiService {

  constructor(private httpClient: HttpClient, private apollo: Apollo,) {}

  // API REST
  getConfig(id_entrypoints: string,uri:string): Observable<any> {
    return this.httpClient.get(
      `${uri}/entrypoints/${id_entrypoints}/questions`
    );
  }

  sendFeedBack(data: any,uri:string): Observable<any> {
    return this.httpClient.post(`${uri}`, data);
  }

  // API GRAPHQL
  getConfigGQ(entrypoint_id: string, language: string): Observable<any> {
    return this.apollo.watchQuery({
      query: gql`query GetQuestionsByEntrypoint {
  entrypoints(where: {id: {_eq: "${entrypoint_id}"}, questions: {}, messages: {language: {_eq: ${language}}}}) {
    id
    rating_positive_min
    is_review_enable
    messages(where: {language: {_eq: ${language}}}) {
      language
      question
      thanks_negative
      thanks_positive
    }
    questions(order_by: {priority: asc}, where: {question: {translations: {language: {_eq: ${language}}}}}) {
      priority
      question {
        id        
        is_positive        
        is_shared
        translations(where: {language: {_eq: ${language}}}) {
          language
          title
        }
        answers(order_by: {priority: asc}, where: {translations: {language: {_eq: ${language}}}}) {
          id
          priority
          translations(where: {language: {_eq: ${language}}}) {
            language
            title
          }
        }        
      }
    }
  }
}`,
    }).valueChanges;
  }
}
