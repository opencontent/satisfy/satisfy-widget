import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Questions } from '../models/config';
import { CdkStep } from '@angular/cdk/stepper';
import { LocalStorageService } from '../local-storage.service';
import {Observable, of, Subject, Subscription} from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: 'app-step-one',
  templateUrl: './step-one.component.html',
  styleUrls: ['./step-one.component.scss'],
})
export class StepOneComponent implements OnInit, OnDestroy {
  @Input() questions: Questions[] = [];
  @Input() answers: any[] = [];
  @Input() selectedStep: number = 1;
  @Input() rateValue: any;
  @Input() stepsLength: number = 2;
  @Input() isRatePositive: boolean = true;
  // @ts-ignore
  @Input() events: Observable<any>;
  // @ts-ignore
  private eventsSubscription: Subscription;

  @Output()
  enableNextStep = new EventEmitter<any>();

  @Input()
  step!: CdkStep;

  @Input()
  md5Path!: string | null;

  stepOneForm: FormGroup;
  hasError = false;
  private unsubscribe$ = new Subject<void>();
  other: boolean = false;
  otherValue = '';


  constructor(
    private fb: FormBuilder,
    private localStorageService: LocalStorageService
  ) {
    this.stepOneForm = this.fb.group({
      inputOption: [null, Validators.required]
    });
  }

  ngOnInit(): void {
    this.stepOneForm.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(selectedValue => {
      this.hasError = !this.stepOneForm.valid;
      this.enableNextStep.emit(this.hasError);
      //Store questions in localstorage
      let localData = JSON.parse(
        // @ts-ignore
        this.localStorageService.getData(`${this.md5Path}`)
      );
      let item = {
        ...localData,
        answers: [
          {
            [this.questions[0].question.id]: selectedValue['inputOption'],
          },
        ],
      };
      this.localStorageService.saveData(
        `${this.md5Path}`,
        JSON.stringify(item)
      );
    });

    this.eventsSubscription = this.events.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.stepOneForm.reset()
    })

  }

  getRadioOptions() {
    return (this.stepOneForm.get('inputOption') as FormControl) || null;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
