import {Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2, ViewChild} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { LocalStorageService } from '../local-storage.service';
import {Observable, of, Subject, Subscription} from "rxjs";
import {takeUntil} from "rxjs/operators";

@Component({
  selector: 'app-step-two',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.scss'],
})
export class StepTwoComponent implements OnInit {
  public stepTwoForm: FormGroup;

  @Input() selectedStep: number = 2;
  @Input() stepsLength: number = 2;
  @Input() rateValue: any;
  @Output()
  detectChangeValueRate = new EventEmitter<any>();

  // @ts-ignore
  private eventsSubscription: Subscription;

  // @ts-ignore
  @Input() events: Observable<any>;

  @Input()
  md5Path!: string | null;

  hasError = false;

  @ViewChild('detailInput') detailInput!: ElementRef;
  @ViewChild('detailInputLabel') detailInputLabel!: ElementRef;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private localStorageService: LocalStorageService,
    private renderer: Renderer2
  ) {
    this.stepTwoForm = this.fb.group({
      details: ['',Validators.maxLength(200)],
    });
  }

  ngOnInit(): void {
    this.stepTwoForm.valueChanges.subscribe(selectedValue => {
//Store questions in localstorage
      let localData = JSON.parse(
        // @ts-ignore
        this.localStorageService.getData(`${this.md5Path}`)
      );
      let item = {
        ...localData,
        review: selectedValue.details,
      };
      this.localStorageService.saveData(
        `${this.md5Path}`,
        JSON.stringify(item)
      );
    });


    this.eventsSubscription = this.events.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.stepTwoForm.reset()
    })
  }


  onFocusIn(): void {
    this.renderer.setAttribute(this.detailInput.nativeElement, 'data-focus-mouse', 'true');
    this.addClass('focus--mouse',this.detailInput.nativeElement)
    this.addClass('active',this.detailInputLabel.nativeElement)

  }
  onFocusOut(): void {
    this.renderer.setAttribute(this.detailInput.nativeElement, 'data-focus-mouse', 'false');
    this.removeClass('focus--mouse',this.detailInput.nativeElement)
    if(!this.detailInput.nativeElement.value){
      this.removeClass('active',this.detailInputLabel.nativeElement)
    }
  }

  addClass(className: string, element: any) {
    this.renderer.addClass(element, className);
  }

  removeClass(className: string, element: any) {
    this.renderer.removeClass(element, className);
  }
}
