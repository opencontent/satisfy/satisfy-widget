import { CdkStepper } from '@angular/cdk/stepper';
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable, Subject, Subscription} from "rxjs";
import {takeUntil} from "rxjs/operators";

/** Custom CDK stepper component */
@Component({
  selector: 'custom-stepper',
  templateUrl: './custom-stepper.component.html',
  styleUrls: ['./custom-stepper.component.scss'],
  providers: [{ provide: CdkStepper, useExisting: CustomStepper }],
})
export class CustomStepper extends CdkStepper implements OnInit {
  @Output()
  submitted = new EventEmitter<any>();
  disabled: boolean = true;

  @Input()
  set enableNextStep(val: boolean) {
    this.disabled = val;
  }

  @Input()
  md5Path!: string | null;

  // @ts-ignore
  @Input() events: Observable<any>;
  // @ts-ignore
  private eventsSubscription: Subscription;

  private unsubscribe$ = new Subject<void>();

  selectStepByIndex(index: number): void {
    this.selectedIndex = index;
  }

  OnchangeStep($event: MouseEvent, selectedIndex: any, send? : boolean) {
    if (send) {
      this.submitted.emit(true);
    }
  }

  ngOnInit() {
    this.eventsSubscription = this.events.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.selectStepByIndex(0)
    })
  }


}
