const fs = require('fs-extra');
const concat = require('concat');
const package_json = require('./package.json');
require('dotenv').config();

var dirStyled = `./elements/bootstrap-italia@2/js`;
var dir = `./elements/js`;

if (!fs.existsSync(dir)){
  fs.mkdirSync(dir, { recursive: true });
}

if (!fs.existsSync(dirStyled)){
  fs.mkdirSync(dirStyled, { recursive: true });
}

(async function build() {
  const files = ['./dist/widget/main.js', './dist/widget/polyfills.js'];
  await fs.ensureDir('elements');
  if(process.env.STYLE == 1){
    await concat(
      files,
      `${dirStyled}/satisfy${
        process.env.ENV == 'DEV' ? `-dev` : ''
      }.js`
    );
  }else{
    await concat(
      files,
      `${dir}/satisfy${
        process.env.ENV == 'DEV' ? `-dev` : ''
      }.js`
    );
  }

})();
